const images = document.querySelectorAll('.img-gallery')//seleccionando  todas las imagenes que tengan la lase img-gallery
const addImage = document.querySelector('.add-image')//seleccionando la imagen que tiene la clase add-image
const containerLigth = document.querySelector('.image-light')//seleccionando el contenedor de la seccion que tiene la clase image-light
const menu1 = document.querySelector('.menu')
//console.log(images)
//console.log(addImage)
//console.log(containerLigth)

images.forEach(img=>{// itere entre en todas las images
    img.addEventListener('click',()=>{
        showimage(img.getAttribute('src'))
    })
})

containerLigth.addEventListener('click',(e)=>{
    if(e.target != addImage){
        containerLigth.classList.toggle('show')
        addImage.classList.toggle('showImage')
        menu1.style.opacity = '1'
    }
})

const showimage = (img)=>{
    addImage.src = img // que del elemento addimage su atributo src va ser igual a la imagen que le va llegar
    containerLigth.classList.toggle('show')
    addImage.classList.toggle('showImage')
    menu1.style.opacity = '0'
}