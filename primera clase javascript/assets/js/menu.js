const menu = document.querySelector(".menu") // de toda pagina seleccionar el elemento que tenga la clase menu
const menuNavegation = document.querySelector(".menu-navigation") // de toda la pagina seleccionar el elemento que tenga la clase menu-navigation
//console.log(menu)
//console.log(menuNavegation)

menu.addEventListener('click',()=>{
    //cada vez que alguien haga click en menu, vamos a;adir al menuNavegation la clase show-menu-navigation
    //toogle  verificara si ya tiene la clase show-menu-navigation
    //y si ya la tiene la va quitar  y si no la va poner

    //alert("escuche que estas haciendo click en el boton de menu")
    menuNavegation.classList.toggle('show-menu-navigation')
})

window.addEventListener('click',e=>{
    //console.log(e.target)
    //verificamos si esta haciendo click en cualquier elemento que 
    // no sea el menu de nevagacion(menuNavegation)
    // y que no sea el boton(menu)
    // y que contenga clase show-menu-navigation
    if(menuNavegation.classList.contains('show-menu-navigation') && e.target != menuNavegation && e.target!=menu){
        menuNavegation.classList.toggle('show-menu-navigation')
    }
})